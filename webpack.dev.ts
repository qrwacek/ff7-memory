import * as path from 'path';
import { Configuration, HotModuleReplacementPlugin } from 'webpack';
import * as merge from 'webpack-merge';

import commonConfig from './webpack.common';

const DEV_SERVER_PORT = 4200;

const devConfig: Configuration = {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        hot: true,
        port: DEV_SERVER_PORT,
        host: '0.0.0.0',
    },
    plugins: [
        new HotModuleReplacementPlugin(),
    ],
};

export default merge(commonConfig, devConfig);

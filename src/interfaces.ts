export interface INameToValueMap {
    [key: string]: any;
}

export interface IGameData {
    symbols: string[];
    time: number;
}

export interface ISize {
    width: number;
    height: number;
}

export interface IPosition {
    x: number;
    y: number;
}

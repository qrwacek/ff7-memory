import { BLACK } from 'const';

/**
 * Maximum game canvas width
 *
 * @type {number}
 */
export const MAX_GAME_WIDTH = 800;

/**
 * Maximum game canvas height
 *
 * @type {number}
 */
export const MAX_GAME_HEIGHT = 600;

/**
 * Game canvas background color
 *
 * @type {number}
 */
export const GAME_BACKGROUND = BLACK;

/**
 * Control button size
 *
 * @type {number}
 */
export const CONTROLS_SIZE = 50;

/**
 * Space between control buttons
 *
 * @type {number}
 */
export const CONTROLS_GUTTER = 10;

/**
 * Controls area background color
 *
 * @type {number}
 */
export const CONTROLS_BACKGROUND = BLACK;

/**
 * Music state key (used for persisting state)
 *
 * @type {string}
 */
export const STATE_MUSIC_OFF = 'musicOff';

/**
 * Sound state key (used for persisting state)
 *
 * @type {string}
 */
export const STATE_SOUND_OFF = 'soundOff';

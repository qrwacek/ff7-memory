/**
 * Directory containing game assets
 *
 * @type {string}
 */
export const ASSETS_ROOT = './assets';

/**
 * Background asset resource key
 *
 * @type {string}
 */
export const BACKGROUND = 'background';

/**
 * Background image file name
 *
 * @type {string}
 */
export const BACKGROUND_IMAGE = 'bg.jpg';

import { INameToValueMap } from 'interfaces';

/**
 * Tile element width (used for calculating tile position)
 *
 * @type {number}
 */
export const TILE_WIDTH = 100;

/**
 * Tile element height (used for calculating tile position)
 *
 * @type {number}
 */
export const TILE_HEIGHT = 100;

/**
 * Space between tiles
 *
 * @type {number}
 */
export const TILES_GUTTER = 10;

/**
 * Maximum number of tiles in one row (will be overridden if tiles do not fit in the screen)
 *
 * @type {number}
 */
export const MAX_TILES_IN_ROW = 2;

/**
 * Duration of tile flip back animation (in milliseconds)
 *
 * @type {number}
 */
export const FLIP_BACK_DELAY = 500;

// SYMBOLS CONFIG
export const SYMBOL_AERITH = 'aerith';
export const SYMBOL_BARRET = 'barret';
export const SYMBOL_CLOUD = 'cloud';
export const SYMBOL_TIFA = 'tifa';
export const SYMBOL_SEPHIROT = 'sephirot';
export const SYMBOL_VINCENT = 'vincent';
export const SYMBOLS: INameToValueMap = {
    symbol00: SYMBOL_AERITH,
    symbol01: SYMBOL_VINCENT,
    symbol04: SYMBOL_BARRET,
    symbol08: SYMBOL_CLOUD,
    symbol14: SYMBOL_TIFA,
    symbol19: SYMBOL_SEPHIROT,
};

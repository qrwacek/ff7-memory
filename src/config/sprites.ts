import { INameToValueMap } from 'interfaces';

/**
 * Tiles sprite resource key
 *
 * @type {string}
 */
export const SPRITE_TILES = 'tiles';

/**
 * Controls sprite resource key
 *
 * @type {string}
 */
export const SPRITE_CONTROLS = 'controls';

/**
 * Sprites file names
 *
 * @type {INameToValueMap}
 */
export const SPRITES: INameToValueMap = {
    [SPRITE_TILES]: 'tiles.json',
    [SPRITE_CONTROLS]: 'controls.json',
};

import { INameToValueMap } from 'interfaces';

/**
 * Game main background music resource key
 *
 * @type {string}
 */
export const SOUND_MAIN = 'main_sound';

/**
 * Success music resource key
 *
 * @type {string}
 */
export const SOUND_SUCCESS = 'success_sound';

/**
 * Tile flip sound effect resource key
 *
 * @type {string}
 */
export const SOUND_FLIP = 'sound_flip';

/**
 * Tiles match sound effect resource key
 *
 * @type {string}
 */
export const SOUND_MATCH = 'sound_match';

/**
 * Sounds file names
 *
 * @type {INameToValueMap}
 */
export const SOUNDS: INameToValueMap = {
    [SOUND_MAIN]: '10-let-the-battles-begin.mp3',
    [SOUND_SUCCESS]: '11-fanfare.mp3',
    [SOUND_FLIP]: 'flip.wav',
    [SOUND_MATCH]: 'match.wav',
};

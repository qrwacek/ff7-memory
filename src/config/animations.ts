import { INameToValueMap } from 'interfaces';

/**
 * Available animation types
 */
export type AnimationType = 'dead' | 'jump' | 'walk';

/**
 * Failure animation resource key
 *
 * @type {string}
 */
export const ANIMATION_FAILURE = 'dead';

/**
 * Success animation resource key
 *
 * @type {string}
 */
export const ANIMATION_SUCCESS = 'jump';

/**
 * Loading animation resource key
 *
 * @type {string}
 */
export const ANIMATION_LOADING = 'walk';

/**
 * Animations file names
 *
 * @type {INameToValueMap}
 */
export const ANIMATIONS: INameToValueMap = {
    [ANIMATION_FAILURE]: 'dead.json',
    [ANIMATION_SUCCESS]: 'jump_fall.json',
    [ANIMATION_LOADING]: 'walk.json',
};

/**
 * Animations frames config
 *
 * @type {INameToValueMap}
 */
export const ANIMATION_FRAMES: INameToValueMap = {
    [ANIMATION_FAILURE]: 10,
    [ANIMATION_SUCCESS]: 16,
    [ANIMATION_LOADING]: 10,
};

/**
 * Animations speed config
 *
 * @type {INameToValueMap}
 */
export const ANIMATION_SPEED: INameToValueMap = {
    [ANIMATION_FAILURE]: 0.2,
    [ANIMATION_SUCCESS]: 0.4,
    [ANIMATION_LOADING]: 0.4,
};

import { IGameData } from 'interfaces';
import { PLAY } from './paths';

export const play = (): Promise<IGameData> => {
    return fetch(PLAY).then((response) => response.json());
};

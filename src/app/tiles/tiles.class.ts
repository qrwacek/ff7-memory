import { MAX_TILES_IN_ROW, SYMBOLS, TILES_GUTTER, TILE_HEIGHT, TILE_WIDTH } from 'config';
import { IPosition } from 'interfaces';

import { ResizableContainer } from '@app/resizable-container';
import { Tile } from '@app/tile';

export class Tiles extends ResizableContainer {
    protected tiles: Tile[] = [];

    constructor(
        protected symbols: string[],
        protected tileClickHandler: () => void,
    ) {
        super();
    }

    public enable(): void {
        this.tiles.forEach((tile: Tile) => {
            tile.enable();
        });
    }

    public disable(): void {
        this.tiles.forEach((tile: Tile) => {
            tile.disable();
        });
    }

    public matched(): boolean {
        return this.tiles.every((tile: Tile) => tile.isMatched);
    }

    protected draw(): void {
        this.drawTiles();
    }

    protected drawTiles(): void {
        this.symbols.forEach((symbol, index) => {
            const { x, y } = this.getTilePosition(index, this.symbols.length);
            if (this.tiles[index]) {
                this.tiles[index].setPosition(x, y);
            } else {
                this.addTile(symbol, x, y);
            }
        });

        this.center();
    }

    /**
     * Get tile position by index for current app screen size
     *
     * @param index Tile index
     * @param total Total number of tiles
     */
    protected getTilePosition(index: number, total: number): IPosition {
        const width = this.screenWidth;
        const height = this.screenHeight;
        const maxTilesRows = Math.floor(height / (TILE_HEIGHT + TILES_GUTTER));

        let tilesInRow = Math.min(
            Math.floor(width / (TILE_WIDTH + TILES_GUTTER)),
            MAX_TILES_IN_ROW,
        );
        let tilesRows = Math.floor(total / tilesInRow);
        if (tilesRows > maxTilesRows) {
            tilesInRow = total / MAX_TILES_IN_ROW;
            tilesRows = Math.floor(total / tilesInRow);

            if (tilesRows > maxTilesRows) {
                tilesInRow = Math.ceil(total / maxTilesRows);
            } else {
                tilesInRow = Math.ceil(total / tilesRows);
            }
        }

        const x = (index % tilesInRow) * (TILE_WIDTH + TILES_GUTTER);
        const y = Math.floor(index / tilesInRow) * (TILE_HEIGHT + TILES_GUTTER);

        return { x, y };
    }

    protected addTile(symbol: string, x: number, y: number): void {
        const tileKey = SYMBOLS[symbol];
        const newTile = new Tile(tileKey, x, y);

        newTile.on('pointerdown', this.tileClickHandler);

        this.addChild(newTile);
        this.tiles.push(newTile);
    }
}

import { Container } from 'pixi.js';

/**
 * Container able to resize on screen size change
 */
export abstract class ResizableContainer extends Container {
    protected screenWidth: number;
    protected screenHeight: number;

    constructor() {
        super();

        this.on('added', () => {
            this.screenWidth = this.parent.width;
            this.screenHeight = this.parent.height;

            this.draw();
        });
    }

    /**
     * Redraw component on screen with new size
     *
     * @param {number} width New screen width
     * @param {number} height New screen height
     */
    public resize(width: number, height: number): void {
        this.screenWidth = width;
        this.screenHeight = height;

        this.draw();
    }

    /**
     * Move component to center of the screen
     *
     * @param {number} offsetX Additional horizontal offset
     * @param {number} offsetY Additional vertical offset
     */
    public center(offsetX: number = 0, offsetY: number = 0): void {
        this.x = (this.screenWidth - this.width) / 2 + offsetX;
        this.y = (this.screenHeight - this.height) / 2 + offsetY;
    }

    /**
     * Draw container content utilising screen size
     */
    protected abstract draw(): void;
}

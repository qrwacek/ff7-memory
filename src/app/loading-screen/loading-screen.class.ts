import { Graphics } from 'pixi.js';

import { AnimatedScreen } from '@app/animated-screen';
import { ANIMATION_LOADING } from 'config';
import { Utils } from 'utils';
import { RED, WHITE } from 'const';

const PROGRESS_BG_COLOR = WHITE;
const PROGRESS_COLOR = RED;
const PROGRESS_MAX_WIDTH = 400;
const PROGRESS_HEIGHT = 10;
const PROGRESS_RADIUS = 4;
const PROGRESS_OFFSET = 130;

export class LoadingScreen extends AnimatedScreen {
    private progressBarBg: Graphics;
    private progressBar: Graphics;
    private progress: number = 1;

    constructor() {
        super(ANIMATION_LOADING, null, { loop: true });
    }

    public setProgress(progress: number): void {
        this.progress = progress;
        this.drawProgressBar();
    }

    public draw(): void {
        super.draw();
        this.drawProgressBarBg();
        this.drawProgressBar();
    }

    private drawProgressBarBg(): void {
        if (this.progressBarBg) {
            Utils.removeElement(this.progressBarBg);
        }

        this.progressBarBg = this.drawBar(this.getProgressBarWidth(), PROGRESS_BG_COLOR);
    }

    private drawProgressBar(): void {
        if (this.progressBar) {
            Utils.removeElement(this.progressBar);
        }

        const progressBarWidth = Math.floor(this.getProgressBarWidth() * this.progress / 100);
        this.progressBar = this.drawBar(progressBarWidth, PROGRESS_COLOR);
    }

    private drawBar(width: number, color: number): Graphics {
        const x = (this.screenWidth - width) / 2;
        const y = this.screenHeight / 2 + PROGRESS_OFFSET;

        const graphics = new Graphics();
        graphics.beginFill(color);
        graphics.drawRoundedRect(x, y, width, PROGRESS_HEIGHT, PROGRESS_RADIUS);

        this.addChild(graphics);

        return graphics;
    }

    private getProgressBarWidth(): number {
        return Math.min(Math.floor(this.screenWidth * 0.8), PROGRESS_MAX_WIDTH);
    }
}

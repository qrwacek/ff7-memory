import { Screen } from '@app/screen';
import { Button } from '@app/button';
import { Animations } from 'utils/animations';

export class PauseScreen extends Screen {
    private button: Button;

    constructor(private onPlayClick: () => void) {
        super();
        this.on('added', this.draw.bind(this));
    }

    public draw(): void {
        super.draw();
        this.drawButton();
    }

    public show(): Promise<any> {
        return Promise.all([
            super.show(),
            Animations.grow(this.button, Screen.FADE_DURATION),
        ]);
    }

    public hide(): Promise<any> {
        return Promise.all([
            super.hide(),
            Animations.shrink(this.button, Screen.FADE_DURATION),
        ]);
    }

    private drawButton(): void {
        if (typeof this.button === 'undefined') {
            const button = new Button('play');
            button.onClick = this.onPlayClick;
            button.anchor.set(0.5);

            this.addChild(button);
            this.button = button;
        }

        this.centerElement(this.button);
    }
}

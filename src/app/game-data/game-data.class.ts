import * as api from 'api';

export class GameData {
    public symbols: string[];
    public time: number;

    public async fetch(): Promise<void> {
        const { symbols, time } = await api.play();
        this.symbols = this.getShuffledSymbols(symbols);
        this.time = time;
    }

    protected getShuffledSymbols(symbols: string[]): string[] {
        const shuffled = [...symbols];
        for (let i = shuffled.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [shuffled[i], shuffled[j]] = [shuffled[j], shuffled[i]];
        }
        return shuffled;
    }
}

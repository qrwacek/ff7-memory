import { Sprite, Texture, loader, ticker } from 'pixi.js';
import { SPRITE_TILES } from 'config';
import { BLACK, WHITE } from 'const';

export class Tile extends Sprite {

    private static getImage(key: string): Texture {
        return loader.resources[SPRITE_TILES].textures[`${key}.jpg`];
    }

    public isMatched: boolean = false;
    public isVisible: boolean = false;

    constructor(private symbol: string, x: number = 0, y: number = 0) {
        super(Tile.getImage(symbol));

        this.anchor.set(0.5);
        this.setPosition(x, y);

        this.interactive = false;
        this.buttonMode = false;

        this.hideImage();
    }

    public setPosition(x: number, y: number): void {
        this.x = x + this.width / 2;
        this.y = y + this.height / 2;
    }

    public enable(): void {
        this.interactive = true;
        this.buttonMode = true;
    }

    public disable(): void {
        this.interactive = false;
        this.buttonMode = false;
    }

    public flip(callback?: () => void): void {
        const flipTicker = new ticker.Ticker();
        let flipped = false;
        flipTicker.add(() => {
            let newScale = this.scale.x + (flipped ? 0.1 : -0.1);
            if (newScale <= 0) {
                if (this.isVisible) {
                    this.hideImage();
                } else {
                    this.showImage();
                }
                flipped = true;
                newScale = 0;
            }

            if (newScale >= 1) {
                flipTicker.stop();
                flipTicker.destroy();
                if (callback) {
                    callback();
                }
                return;
            }

            this.scale.x = newScale;
            this.scale.y = newScale;
        });
        flipTicker.start();
    }

    public isTheSameAs(anotherTile: Tile): boolean {
        return this.symbol === anotherTile.symbol;
    }

    private hideImage(): void {
        this.tint = BLACK;
        this.alpha = 0.8;
        this.isVisible = false;
    }

    private showImage(): void {
        this.tint = WHITE;
        this.alpha = 1;
        this.isVisible = true;
    }
}

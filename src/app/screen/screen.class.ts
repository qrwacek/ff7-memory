import { Container, Graphics } from 'pixi.js';

import { Animations } from 'utils/animations';
import { ResizableContainer } from '@app/resizable-container';
import { Utils } from 'utils';
import { BLACK } from 'const';

const BACKDROP_BG_COLOR = BLACK;
const BACKDROP_ALPHA = 0.8;
const DEFAULT_FADE_DURATION = 300;

/**
 * Screen with semitransparent backdrop
 */
export class Screen extends ResizableContainer {

    protected static FADE_DURATION: number = DEFAULT_FADE_DURATION;

    private backdrop: Graphics;

    public show(): Promise<void> {
        this.visible = true;
        return Animations.fadeIn(this, Screen.FADE_DURATION);
    }

    public hide(): Promise<void> {
        return Animations.fadeOut(this, Screen.FADE_DURATION).then(() => {
            this.visible = false;
        });
    }

    protected draw(): void {
        if (this.backdrop) {
            Utils.removeElement(this.backdrop);
        }

        const graphics = new Graphics();
        graphics.beginFill(BACKDROP_BG_COLOR, BACKDROP_ALPHA);
        graphics.drawRect(0, 0, this.screenWidth, this.screenHeight);

        this.addChildAt(graphics, 0);
        this.backdrop = graphics;
    }

    protected centerElement(element: Container, offsetX: number = 0, offsetY: number = 0): void {
        element.x = this.screenWidth / 2 + offsetX;
        element.y = this.screenHeight / 2 + offsetY;
    }
}

import { Texture, extras } from 'pixi.js';

import { ANIMATION_FRAMES, ANIMATION_SPEED, AnimationType } from 'config';

const DEFAULT_SPEED = 0.4;

export class SpriteAnimation extends extras.AnimatedSprite {

    protected static getFrames(type: AnimationType): Texture[] {
        const noOfFrames: number = ANIMATION_FRAMES[type] || 0;
        const frames = [];
        for (let i = 0; i < noOfFrames; i++) {
            frames.push(Texture.fromFrame(`${type} (${i + 1}).png`));
        }
        return frames;
    }

    constructor(type: AnimationType) {
        super(SpriteAnimation.getFrames(type));

        this.animationSpeed = ANIMATION_SPEED[type] || DEFAULT_SPEED;
        this.scale.set(0.5);
    }

}

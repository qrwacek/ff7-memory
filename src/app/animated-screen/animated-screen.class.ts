import { Text } from 'pixi.js';

import { SpriteAnimation } from '@app/sprite-animation';
import { Screen } from '@app/screen';
import { AnimationType } from 'config';
import { WHITE } from 'const';

interface IAnimationOptions {
    loop?: boolean;
}

const defaultAnimationOptions = {
    loop: false,
};

const TEXT_OFFSET = -130;

/**
 * Screen containing animation and optional text
 */
export class AnimatedScreen extends Screen {
    protected animation: SpriteAnimation;
    protected text: Text;

    constructor(
        private animationType: AnimationType,
        public displayText: string = null,
        protected animationOptions: IAnimationOptions = {},
    ) {
        super();
    }

    public async show(): Promise<void> {
        this.animation.play();
        await super.show();
    }

    public async hide(): Promise<void> {
        await super.hide();
        this.animation.stop();
    }

    protected draw(): void {
        super.draw();

        this.drawAnimation();

        if (this.displayText) {
            this.addText();
        }
    }

    protected drawAnimation(): void {
        if (typeof this.animation === 'undefined') {
            const animationOptions = {
                ...defaultAnimationOptions,
                ...this.animationOptions,
            };
            this.animation = new SpriteAnimation(this.animationType);
            this.animation.anchor.set(0.5);
            this.animation.loop = animationOptions.loop;
            this.animation.play();
            this.addChild(this.animation);
        }

        this.centerElement(this.animation);
    }

    protected addText(): void {
        if (typeof this.text === 'undefined') {
            this.text = new Text(this.displayText, {
                fill: WHITE,
                fontSize: 40,
                fontVariant: 'small-caps',
                align: 'center',
            });
            this.text.anchor.set(0.5);
            this.addChild(this.text);
        } else {
            this.text.text = this.displayText;
        }

        this.centerElement(this.text, 0, TEXT_OFFSET);
    }
}

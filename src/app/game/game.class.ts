import { Application, Container, loader } from 'pixi.js';

import { Timer } from '@app/timer';
import {
    ANIMATIONS,
    ANIMATION_FAILURE,
    ANIMATION_LOADING,
    ANIMATION_SUCCESS,
    BACKGROUND,
    BACKGROUND_IMAGE,
    FLIP_BACK_DELAY,
    GAME_BACKGROUND,
    MAX_GAME_HEIGHT,
    MAX_GAME_WIDTH,
    SOUNDS,
    SPRITES,
} from 'config';
import { HowlerMiddleware } from 'utils/howler-middleware';
import { ISize } from 'interfaces';
import { PauseScreen } from '@app/pause-screen';
import { Tile } from '@app/tile';
import { LoadingScreen } from '@app/loading-screen';
import { Utils } from 'utils';
import { SuccessScreen } from '@app/success-screen';
import { FailureScreen } from '@app/failure-screen';
import { Background } from '@app/background';
import { Tiles } from '@app/tiles';
import { Controls } from '@app/controls';
import { Sounds } from '@app/sounds';
import { GameData } from '@app/game-data';

loader.pre(HowlerMiddleware);

export class Game {
    /**
     * PIXI application instance
     */
    private app: Application;

    /**
     * Game data from the server
     */
    private gameData: GameData;

    /**
     * First flipped tile
     */
    private firstTile: Tile;

    /**
     * Is game currently running
     */
    private isRunning: boolean = false;

    /**
     * Is game in idle state (not clickable during animation)
     */
    private isIdle: boolean = false;

    private content: Container;
    private background: Background;
    private tiles: Tiles;
    private loadingScreen: LoadingScreen;
    private pauseScreen: PauseScreen;
    private successScreen: SuccessScreen;
    private failureScreen: FailureScreen;

    private timer: Timer;
    private controls: Controls;
    private sounds: Sounds;

    /**
     * Initialize PIXI app and add canvas to the DOM
     *
     * @param {Element} domNode DOM node game canvas will be added to
     */
    constructor(private domNode: Element) {
        const size = this.calculateSize();

        // instantiate app
        this.app = new Application({
            width: size.width,
            height: size.height,
            backgroundColor: GAME_BACKGROUND,
            antialias: true,
        });

        this.app.renderer.view.style.position = 'absolute';
        this.app.renderer.view.style.display = 'block';
        this.app.renderer.autoResize = true;

        window.addEventListener('resize', () => {
            this.resize();
        });

        // create view in DOM
        this.domNode.appendChild(this.app.view);
    }

    /**
     * Start loading game
     */
    public init(): void {
        Utils.loadAssets({
            [BACKGROUND]: BACKGROUND_IMAGE,
            [ANIMATION_LOADING]: ANIMATIONS[ANIMATION_LOADING],
        }).then(() => {
            this.load();
        });
    }

    /**
     * Resize game screen and all of its' components.
     * Should be called after viewport size is changed.
     */
    public resize(): void {
        const { width, height } = this.calculateSize();

        if (width === this.app.screen.width && height === this.app.screen.height) {
            return;
        }

        this.app.renderer.resize(width, height);

        if (this.background) {
            this.background.resize(width, height);
        }

        if (this.loadingScreen) {
            this.loadingScreen.resize(width, height);
        }

        if (this.tiles) {
            this.tiles.resize(width, height);
        }

        if (this.pauseScreen) {
            this.pauseScreen.resize(width, height);
        }

        if (this.failureScreen) {
            this.failureScreen.resize(width, height);
        }

        if (this.successScreen) {
            this.successScreen.resize(width, height);
        }

        if (this.controls) {
            this.controls.resize(width, height);
        }
    }

    /**
     * Calculate size of game screen.
     * If viewport is smaller then configured maximum size game goes full-screen.
     *
     * @returns {ISize}
     */
    protected calculateSize(): ISize {
        const viewportWidth = this.domNode.clientWidth;
        const viewportHeight = this.domNode.clientHeight;

        if (viewportWidth >= MAX_GAME_WIDTH && viewportHeight >= MAX_GAME_HEIGHT) {
            return {
                width: MAX_GAME_WIDTH,
                height: MAX_GAME_HEIGHT,
            };
        }

        return {
            width: viewportWidth,
            height: viewportHeight,
        };
    }

    /**
     * Load necessary game assets and start game setup
     */
    protected load(): void {
        this.addMainContainer();
        this.addBackground();

        if (typeof this.loadingScreen === 'undefined') {
            this.loadingScreen = new LoadingScreen();
            this.addElement(this.loadingScreen);
        }

        Utils.loadAssets({
            ...SPRITES,
            [ANIMATION_SUCCESS]: ANIMATIONS[ANIMATION_SUCCESS],
            [ANIMATION_FAILURE]: ANIMATIONS[ANIMATION_FAILURE],
            ...SOUNDS,
        }, (currentLoader) => {
            this.loadingScreen.setProgress(currentLoader.progress);
        }).then(() => {
            this.loadingScreen.hide();
            this.setup();
        });
    }

    protected addMainContainer(): void {
        this.content = new Container();
        this.addMainElement(this.content);
    }

    protected addBackground(): void {
        if (this.background) {
            this.removeElement(this.background);
        }

        this.background = new Background(this.app.screen.width, this.app.screen.height);
        this.addElement(this.background, 0);
    }

    /**
     * Load game data, initialize game components and show pause screen
     */
    protected async setup(): Promise<void> {
        await this.fetchGameData();

        this.setupTiles();
        this.setupTimer();
        this.setupPauseScreen();
        this.setupSounds();
        this.setupControls();
    }

    protected async fetchGameData(): Promise<void> {
        this.gameData = new GameData();
        await this.gameData.fetch();
    }

    protected setupTiles(): void {
        if (this.tiles) {
            this.removeElement(this.tiles);
        }
        this.tiles = new Tiles(this.gameData.symbols, this.createTileClickHandler());
        this.addElement(this.tiles, 1);
    }

    protected setupTimer(): void {
        if (this.timer) {
            this.removeElement(this.timer);
        }
        this.timer = new Timer(this.gameData.time, this.gameOver.bind(this));
        this.addElement(this.timer);
    }

    protected setupSounds(): void {
        if (typeof this.sounds === 'undefined') {
            this.sounds = new Sounds();
        }
    }

    protected setupControls(): void {
        if (typeof this.controls === 'undefined') {
            this.controls = new Controls();
            this.controls.musicToggleHandler = () => {
                this.sounds.toggleMusic();
            };
            this.controls.soundToggleHandler = () => {
                this.sounds.toggleSound();
            };
            this.addMainElement(this.controls);
        }
    }

    protected setupPauseScreen(): void {
        if (this.pauseScreen) {
            this.pauseScreen.show();
        } else {
            this.pauseScreen = new PauseScreen(this.start.bind(this));
            this.addMainElement(this.pauseScreen);
        }
    }

    protected addMainElement(element: Container): void {
        this.app.stage.addChild(element);
    }

    protected addElement(element: any, index?: number): void {
        if (typeof index !== 'undefined') {
            this.content.addChildAt(element, index);
        } else {
            this.content.addChild(element);
        }
    }

    protected removeElement(element: Container): void {
        this.app.stage.removeChild(element);
        element.destroy();
    }

    protected createTileClickHandler(): () => void {
        const game = this;

        return function tileClickHandler(): void {
            const tile: Tile = this;

            if (!game.isRunning || game.isIdle || tile.isMatched || tile.isVisible) { return; }

            game.isIdle = true;
            game.sounds.playFlipSound();
            tile.flip(() => {
                if (game.firstTile) {
                    if (game.firstTile.isTheSameAs(tile)) {
                        game.sounds.playMatchSound();
                        game.firstTile.isMatched = true;
                        tile.isMatched = true;

                        game.firstTile = null;
                        game.isIdle = false;

                        if (game.isCompleted()) {
                            game.completed();
                        }
                    } else {
                        setTimeout(() => {
                            game.firstTile.flip();
                            tile.flip(() => {
                                game.firstTile = null;
                                game.isIdle = false;
                            });
                        }, FLIP_BACK_DELAY);
                    }
                } else {
                    game.firstTile = tile;
                    game.isIdle = false;
                }
            });
        };
    }

    protected async start(): Promise<void> {
        await this.pauseScreen.hide();
        this.isRunning = true;
        this.tiles.enable();
        this.timer.start();
        this.sounds.playMusic();
    }

    // protected pause(): void {
    //     this.stop();
    //     this.pauseScreen.show();
    // }

    protected stop(): void {
        this.isRunning = false;
        this.tiles.disable();
        this.timer.pause();
        this.sounds.stopMusic();
    }

    protected isCompleted(): boolean {
        return this.tiles.matched();
    }

    protected async completed(): Promise<void> {
        this.stop();
        if (typeof this.successScreen === 'undefined') {
            this.successScreen = new SuccessScreen();
            this.addElement(this.successScreen);
        }
        await this.successScreen.show();
        this.sounds.playSuccessMusic();
        await Utils.wait(5);
        this.sounds.stopSuccessMusic();
        this.reset();
    }

    protected async gameOver(): Promise<void> {
        this.stop();
        if (typeof this.failureScreen === 'undefined') {
            this.failureScreen = new FailureScreen();
            this.addElement(this.failureScreen);
        }
        await this.failureScreen.show();
        await Utils.wait(5);
        this.reset();
    }

    protected reset(): void {
        this.isRunning = false;
        this.isIdle = false;
        this.firstTile = null;

        if (this.successScreen) {
            this.successScreen.hide();
        }
        if (this.failureScreen) {
            this.failureScreen.hide();
        }

        this.setup();
    }
}

import { ANIMATION_SUCCESS } from 'config';
import { AnimatedScreen } from '@app/animated-screen';

export class SuccessScreen extends AnimatedScreen {
    constructor() {
        super(ANIMATION_SUCCESS, 'Congratulations!', {
            loop: true,
        });
    }

    protected drawAnimation(): void {
        super.drawAnimation();

        this.animation.onFrameChange = () => {
            this.animation.y += (this.animation.currentFrame < 8 ? -10 : 10);
        };
    }
}

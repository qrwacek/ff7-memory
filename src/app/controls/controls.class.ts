import { Graphics } from 'pixi.js';
import * as ls from 'local-storage';

import { ResizableContainer } from '@app/resizable-container';
import { BUTTON_MUSIC, BUTTON_SOUND, Button } from '@app/button';
import { CONTROLS_BACKGROUND, CONTROLS_GUTTER, CONTROLS_SIZE, STATE_MUSIC_OFF, STATE_SOUND_OFF } from 'config';
import { Utils } from 'utils';

export class Controls extends ResizableContainer {
    public musicToggleHandler: () => void;
    public soundToggleHandler: () => void;

    protected background: Graphics;
    protected musicButton: Button;
    protected soundButton: Button;

    protected draw(): void {
        this.drawBackground();
        this.drawMusicButton();
        this.drawSoundButton();
        this.setPosition();
    }

    protected drawBackground(): void {
        if (this.background) {
            Utils.removeElement(this.background);
        }

        const bgWidth = CONTROLS_SIZE + 2 * CONTROLS_GUTTER;
        const bgHeight = 2 * CONTROLS_SIZE + 3 * CONTROLS_GUTTER;

        this.background = new Graphics()
            .beginFill(CONTROLS_BACKGROUND)
            .drawRect(0, 0, bgWidth, bgHeight)
            .endFill();
        this.addChildAt(this.background, 0);
    }

    protected drawMusicButton(): void {
        if (typeof this.musicButton === 'undefined') {
            this.musicButton = new Button(BUTTON_MUSIC);
            if (ls(STATE_MUSIC_OFF)) {
                this.musicButton.disabled = true;
            }
            this.musicButton.onClick = () => {
                this.musicButton.disabled = !this.musicButton.disabled;
                this.musicToggleHandler();
            };
            this.addChild(this.musicButton);

            this.musicButton.width = CONTROLS_SIZE;
            this.musicButton.height = CONTROLS_SIZE;
        }

        this.musicButton.x = CONTROLS_GUTTER;
        this.musicButton.y = CONTROLS_GUTTER;
    }

    protected drawSoundButton(): void {
        if (typeof this.soundButton === 'undefined') {
            this.soundButton = new Button(BUTTON_SOUND);
            if (ls(STATE_SOUND_OFF)) {
                this.soundButton.disabled = true;
            }
            this.soundButton.onClick = () => {
                this.soundButton.disabled = !this.soundButton.disabled;
                this.soundToggleHandler();
            };
            this.addChild(this.soundButton);

            this.soundButton.width = CONTROLS_SIZE;
            this.soundButton.height = CONTROLS_SIZE;
        }

        this.soundButton.x = CONTROLS_GUTTER;
        this.soundButton.y = CONTROLS_SIZE + 2 * CONTROLS_GUTTER;
    }

    /**
     * Position in top right corner
     */
    protected setPosition(): void {
        this.x = this.screenWidth - this.width;
    }
}

import { Container, Graphics, Sprite } from 'pixi.js';

import { Utils } from 'utils';
import { BACKGROUND } from 'config';
import { BLACK } from 'const';

/**
 * Background with cover effect
 *
 * @see https://gist.github.com/ClickSimply/581823db9cdc8d94ed3f78c1a548f50d
 */
export class Background extends Container {
    protected background: Sprite;

    constructor(protected screenWidth: number, protected screenHeight: number) {
        super();

        this.on('added', () => {
            this.draw();
        });
    }

    public resize(width: number, height: number): void {
        this.screenWidth = width;
        this.screenHeight = height;
        this.draw();
    }

    protected draw(): void {
        this.drawMask();
        this.drawBackground();
    }

    protected drawMask(): void {
        if (this.mask) {
            Utils.removeElement(this.mask);
        }
        this.mask = new Graphics()
            .beginFill(BLACK)
            .drawRect(0, 0, this.screenWidth, this.screenHeight)
            .endFill();
        this.addChild(this.mask);
    }

    protected drawBackground(): void {
        if (this.background) {
            Utils.removeElement(this.background);
        }
        this.background = new Sprite(Utils.getAsset(BACKGROUND));
        this.addChild(this.background);

        this.resizeBackground();
    }

    protected resizeBackground(): void {
        const screenRatio = this.screenWidth / this.screenHeight;
        const backgroundRatio = this.background.width / this.background.height;

        let scale = 1;
        let offsetX = 0;
        let offsetY = 0;
        if (screenRatio > backgroundRatio) {
            // photo is wider than background
            scale = this.screenWidth / this.background.width;
            offsetY = (this.screenHeight - this.background.height * scale) / 2;
        } else {
            // photo is taller than background
            scale = this.screenHeight / this.background.height;
            offsetX = (this.screenWidth - this.background.width * scale) / 2;
        }

        this.background.scale.set(scale);
        this.background.position.set(offsetX, offsetY);
    }
}

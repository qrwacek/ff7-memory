import { AnimatedScreen } from '@app/animated-screen';
import { ANIMATION_FAILURE } from 'config';

export class FailureScreen extends AnimatedScreen {
    constructor() {
        super(ANIMATION_FAILURE, 'You failed :(');
    }
}

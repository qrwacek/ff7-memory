import { Sprite, Texture, loader } from 'pixi.js';
import { ButtonType } from '@app/button';
import { SPRITE_CONTROLS } from 'config';

const STATE_DEFAULT = '';
const STATE_HOVER = 'hover';
const STATE_CLICK = 'click';
const STATE_DISABLED = 'disabled';

type ButtonState = '' | 'hover' | 'click' | 'disabled';

/**
 * Class for creating buttons with states (default, hover, click , disabled).
 */
export class Button extends Sprite {

    private static getImage(key: string): Texture {
        return loader.resources[SPRITE_CONTROLS].textures[`${key}.png`];
    }

    /**
     * Button click handler
     */
    public onClick: () => void;

    private type: ButtonType;
    private isDisabled: boolean;

    constructor(type: ButtonType) {
        const textureButton = Button.getImage(type);

        super(textureButton);

        this.type = type;

        this
            .on('pointerover', () => {
                this.setState(STATE_HOVER);
            })
            .on('pointerout', () => {
                this.setState(STATE_DEFAULT);
            })
            .on('pointerupoutside', () => {
                this.setState(STATE_DEFAULT);
            })
            .on('pointerdown', () => {
                this.setState(STATE_CLICK);
            })
            .on('pointerup', () => {
                this.setState(STATE_DEFAULT);
                if (this.onClick) {
                    this.onClick();
                }
            });

        this.interactive = true;
        this.buttonMode = true;
    }

    private setState(state: ButtonState): void {
        this.texture = this.getStateImage(this.isDisabled ? STATE_DISABLED : state);
    }

    private getStateImage(state: ButtonState): Texture {
        return Button.getImage(state === STATE_DEFAULT ? this.type : `${this.type}_${state}`);
    }

    public get disabled(): boolean {
        return this.isDisabled;
    }

    public set disabled(isDisabled: boolean) {
        this.isDisabled = isDisabled;

        this.texture = this.getStateImage(isDisabled ? STATE_DISABLED : STATE_DEFAULT);
    }
}

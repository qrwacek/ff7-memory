import { Container, Graphics, Text, ticker } from 'pixi.js';
import { WHITE } from 'const';
import { Utils } from 'utils';
import { CONTROLS_BACKGROUND, CONTROLS_GUTTER, CONTROLS_SIZE } from 'config';

const SECOND_MS = 1000;

const TEXT_COLOR = WHITE;

export class Timer extends Container {
    protected static getSize(): number {
        return CONTROLS_SIZE + 2 * CONTROLS_GUTTER;
    }

    private ticker: ticker.Ticker;
    private elapsedTimeMS: number = 0;
    private elapsedTime: number = 0;

    private background: Graphics;
    private remainingTimeText: Text;

    /**
     * Create timer text and set up ticker
     *
     * @param time Timer time in seconds
     * @param onTimePassed Function called when the time has passed
     */
    constructor(public time: number, private onTimePassed: () => void) {
        super();

        this.drawBackground();
        this.drawText();
        this.setUpTicker();
    }

    public start(): void {
        this.ticker.start();
    }

    public pause(): void {
        this.ticker.stop();
    }

    protected drawBackground(): void {
        if (this.background) {
            Utils.removeElement(this.background);
        }

        const bgSize = Timer.getSize();

        this.background = new Graphics()
            .beginFill(CONTROLS_BACKGROUND)
            .drawRect(0, 0, bgSize, bgSize)
            .endFill();
        this.addChildAt(this.background, 0);
    }

    protected drawText(): void {
        const text = new Text(`${this.time}`, {
            fill: TEXT_COLOR,
            fontSize: 30,
            align: 'center',
        });

        this.addChild(text);
        this.remainingTimeText = text;

        const center = Timer.getSize() / 2;

        this.remainingTimeText.anchor.set(0.5);
        this.remainingTimeText.position.set(center, center);
    }

    protected setUpTicker(): void {
        this.ticker = new ticker.Ticker();
        this.ticker.autoStart = false;
        this.ticker.add(() => {
            this.elapsedTimeMS += this.ticker.elapsedMS;

            const elapsedTime = Math.floor(this.elapsedTimeMS / SECOND_MS);
            if (elapsedTime !== this.elapsedTime) {
                this.elapsedTime = elapsedTime;
                this.updateTimer();
            }

            if (this.elapsedTime >= this.time) {
                this.onTimePassed();
                this.pause();
            }
        });
    }

    protected updateTimer(): void {
        const remainingTime = Math.max(this.time - this.elapsedTime, 0);
        this.remainingTimeText.text = `${remainingTime}`;
    }
}

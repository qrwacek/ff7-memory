import * as ls from 'local-storage';

import { Utils } from 'utils';
import { SOUND_FLIP, SOUND_MAIN, SOUND_MATCH, SOUND_SUCCESS, STATE_MUSIC_OFF, STATE_SOUND_OFF } from 'config';

export class Sounds {
    protected music: Howl;
    protected successMusic: Howl;

    protected flipSound: Howl;
    protected matchSound: Howl;

    protected musicOn: boolean = true;
    protected soundOn: boolean = true;

    constructor() {
        this.music = Utils.getSound(SOUND_MAIN);
        this.music.volume(0.8);
        this.successMusic = Utils.getSound(SOUND_SUCCESS);
        this.flipSound = Utils.getSound(SOUND_FLIP);
        this.matchSound = Utils.getSound(SOUND_MATCH);

        if (ls.get(STATE_MUSIC_OFF)) {
            this.toggleMusic();
        }
        if (ls.get(STATE_SOUND_OFF)) {
            this.toggleSound();
        }
    }

    public playMusic(): void {
        this.music.play();
    }

    public stopMusic(): void {
        this.music.stop();
    }

    public playSuccessMusic(): void {
        this.successMusic.play();
    }

    public stopSuccessMusic(): void {
        this.successMusic.stop();
    }

    public toggleMusic(): boolean {
        if (this.musicOn) {
            ls.set(STATE_MUSIC_OFF, true);
        } else {
            ls.remove(STATE_MUSIC_OFF);
        }
        this.musicOn = !this.musicOn;
        this.music.mute(!this.musicOn);
        this.successMusic.mute(!this.musicOn);
        return this.musicOn;
    }

    public playFlipSound(): void {
        this.flipSound.play();
    }

    public playMatchSound(): void {
        this.matchSound.play();
    }

    public toggleSound(): boolean {
        if (this.soundOn) {
            ls.set(STATE_SOUND_OFF, true);
        } else {
            ls.remove(STATE_SOUND_OFF);
        }
        this.soundOn = !this.soundOn;
        this.flipSound.mute(!this.soundOn);
        this.matchSound.mute(!this.soundOn);
        return this.soundOn;
    }
}

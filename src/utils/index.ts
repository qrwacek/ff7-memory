import { centerElement, centerSprite } from './center-element';
import { getAsset } from './get-asset';
import { getSound } from './get-sound';
import { loadAssets } from './load-assets';
import { removeElement } from './remove-element';
import { wait } from './wait';

export const Utils = {
    centerElement,
    centerSprite,
    getAsset,
    getSound,
    loadAssets,
    removeElement,
    wait,
};

import { Texture, loader } from 'pixi.js';

export const getAsset = (key: string): Texture => {
    return loader.resources[key].texture;
};

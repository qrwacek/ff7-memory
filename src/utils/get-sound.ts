import { loader } from 'pixi.js';

export const getSound = (key: string): Howl => {
    return loader.resources[key].data;
};

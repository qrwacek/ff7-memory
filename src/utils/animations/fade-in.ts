import { Container } from 'pixi.js';

import { animate } from './animate';

export const fadeIn = (object: Container, duration?: number) =>
    animate(object, { alpha: 1 }, duration);

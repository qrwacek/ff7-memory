import { Easing, Tween, update } from '@tweenjs/tween.js';
import { ticker } from 'pixi.js';

ticker.shared.add(function(): void {
    update(this.elapsedMS);
});

export const DEFAULT_DURATION = 300;

/**
 * Animate object properties
 *
 * @param object Object to animate
 * @param properties Properties to be animated
 * @param {number} duration Animation duration
 * @returns {Promise<void>} Promise resolved when animation is complete
 */
export const animate = (object: any, properties: any, duration: number = DEFAULT_DURATION): Promise<void> => {
    const tween = new Tween(object);
    tween.to(properties, duration).easing(Easing.Quadratic.Out);

    return new Promise((resolve) => {
        tween.onComplete(() => {
            resolve();
        });
        tween.start();
    });
};

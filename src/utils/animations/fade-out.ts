import { Container } from 'pixi.js';

import { animate } from './animate';

export const fadeOut = (object: Container, duration?: number) =>
    animate(object, { alpha: 0 }, duration);

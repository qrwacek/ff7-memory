import { Container } from 'pixi.js';

import { animate } from './animate';

export const grow = (object: Container, duration?: number) =>
    animate(object.scale, { x: 1, y: 1 }, duration);

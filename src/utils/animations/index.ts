import { animate } from './animate';
import { fadeOut } from './fade-out';
import { fadeIn } from './fade-in';
import { shrink } from './shrink';
import { grow } from './grow';

export const Animations = {
    animate,
    fadeIn,
    fadeOut,
    grow,
    shrink,
};

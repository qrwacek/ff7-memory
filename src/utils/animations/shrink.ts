import { Container } from 'pixi.js';

import { animate } from './animate';

export const shrink = (object: Container, duration?: number) =>
    animate(object.scale, { x: 0, y: 0 }, duration);

import { DisplayObject } from 'pixi.js';

export const removeElement = (element: DisplayObject): void => {
    if (this.parent) {
        this.parent.removeChild(element);
    }
    element.destroy();
};

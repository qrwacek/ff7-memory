import { Container, Sprite } from 'pixi.js';

/**
 * Center element within its' container
 *
 * @param {Container} element Element to center
 * @param {number} offsetX Additional horizontal offset
 * @param {number} offsetY Additional vertical offset
 */
export const centerElement = (element: Container, offsetX: number = 0, offsetY: number = 0): void => {
    if (!element.parent) { return; }

    const { width: parentWidth, height: parentHeight } = element.parent;
    const { width, height } = element;

    element.x = (parentWidth - width) / 2 + offsetX;
    element.y = (parentHeight - height) / 2 + offsetY;
};

/**
 * Center sprite within its' container.
 * Supports sprite anchor.
 *
 * @param {Sprite} element
 */
export const centerSprite = (element: Sprite): void => {
    const offsetX = element.anchor.x * element.width;
    const offsetY = element.anchor.y * element.height;

    centerElement(element, offsetX, offsetY);
};

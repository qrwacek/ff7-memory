import { loaders } from 'pixi.js';
import { Howl } from 'howler';

const soundsExtensions = ['wav', 'ogg', 'mp3', 'mpeg'];

/**
 * @link https://github.com/seleb/HowlerPixiLoaderMiddleware
 */
export function HowlerMiddleware(resource: any, next: () => void): void {
    if (resource && soundsExtensions.includes(resource.extension)) {
        resource._setFlag(loaders.Resource.STATUS_FLAGS.LOADING, true);
        const options: IHowlProperties = JSON.parse(JSON.stringify(resource.metadata));
        options.src = [resource.url];
        options.onload = () => {
            resource.complete();
            next();
        };
        options.onloaderror = (_, error: any) => {
            resource.abort(error);
            next();
        };
        resource.data = new Howl(options);
    } else {
        next();
    }
}

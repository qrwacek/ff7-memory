/**
 * Function waiting for selected number of seconds
 *
 * @param {number} seconds Seconds to wait
 * @returns {Promise<void>} Promise resolved after selected number of seconds
 */
export function wait(seconds: number): Promise<void> {
    return new Promise((resolve) => {
        setTimeout(() => resolve(), 1000 * seconds);
    });
}

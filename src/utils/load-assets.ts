import { loader, loaders } from 'pixi.js';

import { INameToValueMap } from 'interfaces';
import { ASSETS_ROOT } from 'config';

export function loadAssets(assets: INameToValueMap, onProgress?: (loader: loaders.Loader) => void): Promise<void> {
    Object.entries(assets).forEach(([key, file]) => {
        loader.add(key, `${ASSETS_ROOT}/${file}`);
    });
    if (onProgress) {
        loader.on('progress', onProgress);
    }
    return new Promise((resolve) => {
        loader.load(() => {
            resolve();
        });
    });
}

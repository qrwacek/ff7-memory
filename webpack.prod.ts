import { Configuration } from 'webpack';
import * as merge from 'webpack-merge';

import commonConfig from './webpack.common';

const prodConfig: Configuration = {
    mode: 'production',
};

export default merge(commonConfig, prodConfig);

# FF7 Memory

Final Fantasy VII tribute memory game for GiG Games technical challenge

## Installation

Clone the repository.

```
git clone git@bitbucket.org:qrwacek/ff7-memory.git
cd ff7-memory
```

Install app dependencies

```
npm install
```

## Running

To start the application in development mode run

```
npm start
```

This will start webpack development server on http://localhost:4200

## Building

To build production ready version of the application run

```
npm run build
```

This will create app bundle in `/dist` folder

## Directory Layout

```
src/                  --> all of the source files for the application
  api/                --> API communication components
  app/                --> game components
  assets/             --> images, sounds and animations used by game
  config/             --> app configuration
  utils/              --> utilities used across the app
  app.ts              --> main application entrypoint
  const.ts            --> constants used in the app
  game.json           --> mock file for API response
  index.html          --> HTML skeleton for app bundle
  interfaces.ts       --> interfaces used in the app
webpack.common.ts     --> shared configuration for webpack
webpack.dev.ts        --> development configuration for webpack
webpack.prod.ts       --> production configuration for webpack
```
